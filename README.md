prettify_bash
=============

Code formatter / beautifier for bash written in python by
Paul Lutus (a remake of previous version in Ruby)
and patched by Dominik Kummer to also prettify complex Makefiles.

For further details please see the following blog record
http://arachnoid.com/python/beautify_bash_program.html


Just apply the patch like this:
```
patch beautify_bash.py < prettify_bash.diff
```

## Caution

Never apply the prettifier on original scripts without any backups!
